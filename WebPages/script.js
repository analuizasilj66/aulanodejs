function changeTab(tabName) {
    // Mapeia o nome da guia para o arquivo correspondente
    const tabMapping = {
        'home': 'conteudo_home.html',
        'objetivo': 'conteudo_objetivo.html',
        'desenvolvimento': 'conteudo_desenvolvimento.html',
        'conclusao': 'conteudo_conclusao.html'
    };

    // Carrega dinamicamente o conteúdo da guia selecionada
    fetch(tabMapping[tabName])
        .then(response => response.text())
        .then(data => {
            document.getElementById('content').innerHTML = data;
        });
}